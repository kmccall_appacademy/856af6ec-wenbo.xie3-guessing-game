# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  correct_number = rand(1..100)
  guess_count = 1

  print "Guess a number between 1 and 100: "
  input = Integer(gets.chomp)

  until input == correct_number
    guess_count += 1

    if input > correct_number
      puts "#{input} is too high!"
    else
      puts "#{input} is too low!"
    end

    print "Guess a number between 1 and 100: "
    input = Integer(gets.chomp)
  end

  puts "#{correct_number} is the correct number! You took #{guess_count} guesses."
end

def shuffle_file
  print "Enter a file name to shuffle: "
  file_input = gets.chomp
  file_lines = File.readlines(file_input)

  File.open("#{file_input}-shuffled.txt", "w") do |f|
    f.puts(file_lines.shuffle)
  end
end

if __FILE__ == $PROGRAM_NAME
  # guessing_game
  shuffle_file
end
